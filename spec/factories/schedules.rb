FactoryGirl.define do
    factory :schedule do
        latitude 0.00
        longitude 0.00
        from Time.new
        to Time.new.advance(hours: 1)
    end
end
