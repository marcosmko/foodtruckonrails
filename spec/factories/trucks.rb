FactoryGirl.define do
    factory :truck do
        name { FFaker::Name.name }
    end
end
