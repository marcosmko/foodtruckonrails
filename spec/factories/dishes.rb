FactoryGirl.define do
    factory :dish do
        name { FFaker::Name.name }
        description "descrição"
        price 0.00
    end
end
