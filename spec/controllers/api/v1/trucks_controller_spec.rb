require 'rails_helper'
require 'pp'

RSpec.describe Api::V1::TrucksController, type: :controller do
    
    describe "GET #showall" do
        before(:each) do
            @truck = FactoryGirl.create :truck
            get :show
        end
        
        after(:each) do
            @truck.destroy
        end
        
        it "returns the information about a reporter on a hash" do
            user_response = json_response
            expect(user_response.count).to be > 0
        end
        
        it { should respond_with 200 }
    end
    
    describe "GET #show" do
        context "when truck exists" do
            before(:each) do
                @truck = FactoryGirl.create :truck
                get :show, id: @truck.id, format: :json
            end
            
            after(:each) do
                @truck.destroy
            end
            
            it "returns the information about a reporter on a hash" do
                user_response = json_response
                expect(user_response[:name]).to eql @truck.name
            end
            
            it { should respond_with 200 }
        end
        
        context "when truck does not exist" do
            before(:each) do
                get :show, id: "INVALIDID", format: :json
            end
            
            it { should respond_with 404 }
        end
    end
    
    describe "GET #search" do
        before(:each) do
            @truck = FactoryGirl.create :truck
            get :show, name: @truck.name[0], format: :json
        end
        
        after(:each) do
            @truck.destroy
        end
        
        it "returns the information about a reporter on a hash" do
            user_response = json_response
            expect(user_response.count).to be > 0
        end
        
        it { should respond_with 200 }
    end
    
    describe "PUT/PATCH #update" do
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @truck = FactoryGirl.create :truck
            
            @token.create_rel("FOR", @user)
            @user.create_rel("OWN", @truck)
            
            api_authorization_header @token.auth_token
        end
        
        after(:each) do
            @user.destroy
        end
        
        context "when is successfully updated" do
            before(:each) do
                patch :update, { id: @truck.id, name: "NEW NAME TRUCK",  description: "NEW DESCRIPTION" }, format: :json
            end
            
            it { should respond_with 204 }
        end
        
        context "when is not created" do
            before(:each) do
                patch :update, { id: @truck.id, name: "" }, format: :json
            end
            
            it { should respond_with 422 }
        end
        
        context "when user does not have authorization" do
            before(:each) do
                @another_truck = FactoryGirl.create :truck
                patch :update, { id: @another_truck.id, name: "NEW NAME TRUCK" }, format: :json
            end
            
            after(:each) do
                @another_truck.destroy
            end
            
            it { should respond_with 403 }
        end
    end
    
    
    
end
