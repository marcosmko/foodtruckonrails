require 'rails_helper'
require 'pp'

RSpec.describe Api::V1::SchedulesController, type: :controller do
    
    describe "GET #show" do
        before(:each) do
            @truck = FactoryGirl.create :truck
            @schedule = FactoryGirl.create :schedule
            
            @truck.create_rel("WITH", @schedule)
            
            get :show
        end
        
        after(:each) do
            @truck.destroy
        end
        
        it "returns the information about a reporter on a hash" do
            user_response = json_response
            expect(user_response[:trucks].count).to be > 0
        end
        
        it { should respond_with 200 }
    end
    
    describe "POST #create" do
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @truck = FactoryGirl.create :truck
            
            @token.create_rel("FOR", @user)
            @user.create_rel("OWN", @truck)
            
            api_authorization_header @token.auth_token
        end
        
        after(:each) do
            @user.destroy
        end
        
        context "when is successfully created" do
            before(:each) do
                @schedule_attributes = FactoryGirl.attributes_for :schedule
                post :create, {id: @truck.id, schedule: @schedule_attributes}, format: :json
            end
            
            it "renders an errors json" do
                user_response = json_response
                expect(user_response).to have_key(:id)
            end
            
            it { should respond_with 201 }
        end
        
        context "when truck not found" do
            before(:each) do
                @truck = FactoryGirl.create :truck
                @schedule_attributes = FactoryGirl.attributes_for :schedule
                post :create, {id: @truck.id, schedule: @schedule_attributes}, format: :json
            end
            
            after(:each) do
                @truck.destroy
            end
            
            it { should respond_with 403 }
        end
    end
    
    describe "PUT/PATCH #update" do
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @truck = FactoryGirl.create :truck
            @schedule = FactoryGirl.create :schedule
            
            @token.create_rel("FOR", @user)
            @user.create_rel("OWN", @truck)
            @truck.create_rel("WITH", @schedule)
            
            api_authorization_header @token.auth_token
        end
        
        after(:each) do
            @user.destroy
        end
        
        context "when is successfully updated" do
            before(:each) do
                patch :update, { id: @schedule.id, latitude: 30 }, format: :json
            end
            
            it { should respond_with 204 }
        end
        
        context "when is not created" do
            before(:each) do
                patch :update, { id: @schedule.id, from: "incorrect format" }, format: :json
            end
            
            it { should respond_with 422 }
        end
        
        context "when user does not have authorization" do
            before(:each) do
                @another_schedule = FactoryGirl.create :schedule
                patch :update, { id: @another_schedule.id, longitude: 30 }, format: :json
            end
            
            after(:each) do
                @another_schedule.destroy
            end
            
            it { should respond_with 403 }
        end
    end
    
    describe "DELETE #destroy" do
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @truck = FactoryGirl.create :truck
            
            @token.create_rel("FOR", @user)
            @user.create_rel("OWN", @truck)
            
            api_authorization_header @token.auth_token
        end
        
        after(:each) do
            @user.destroy
        end
        
        context "when is succesfully destroyed" do
            before(:each) do
                @schedule = FactoryGirl.create :schedule
                @truck.create_rel("WITH", @schedule)
                delete :destroy, { id: @schedule.id }, format: :json
            end
            
            it { should respond_with 204 }
        end
        
        context "when user does not have authorization" do
            before(:each) do
                @schedule = FactoryGirl.create :schedule
                delete :destroy, { id: @schedule.id }, format: :json
            end
            
            after(:each) do
                @schedule.destroy
            end
            
            it { should respond_with 403 }
        end
        
    end
    
end
