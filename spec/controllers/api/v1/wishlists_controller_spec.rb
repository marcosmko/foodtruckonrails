require 'rails_helper'

RSpec.describe Api::V1::WishlistsController, type: :controller do
    
    describe "GET #show" do
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @truck = FactoryGirl.create :truck
            
            @token.create_rel("FOR", @user)
            @user.wishlist << @truck
            
            api_authorization_header @token.auth_token
            
            get :show
        end
        
        after(:each) do
            @user.destroy
            @truck.destroy
        end
        
        it "returns the information about a reporter on a hash" do
            user_response = json_response
            expect(user_response[:trucks][0][:id]).to eq(@truck.id)
        end
        
        it { should respond_with 200 }
    end
    
    describe "POST #create" do
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @truck = FactoryGirl.create :truck
            
            @token.create_rel("FOR", @user)
            
            api_authorization_header @token.auth_token
        end
        
        after(:each) do
            @user.destroy
            @truck.destroy
        end
        
        context "when is successfully created" do
            before(:each) do
                post :create, { id: @truck.id }
            end
            
            it { should respond_with 201 }
        end
    end
    
    describe "DELETE #destroy" do
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @truck = FactoryGirl.create :truck
            
            @user.wishlist << @truck
            @token.create_rel("FOR", @user)
            
            api_authorization_header @token.auth_token
        end
        
        after(:each) do
            @user.destroy
            @truck.destroy
        end
        
        context "when is succesfully destroyed" do
            before(:each) do
                delete :destroy, { id: @truck.id }
            end
            
            it { should respond_with 204 }
        end
        
    end
    
end
