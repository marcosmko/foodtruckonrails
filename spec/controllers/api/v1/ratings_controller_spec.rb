require 'rails_helper'
require 'pp'

RSpec.describe Api::V1::RatingsController, type: :controller do
    
    describe "GET #show for user" do
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @truck = FactoryGirl.create :truck
            
            @token.create_rel("FOR", @user)
            
            @rate = Rate.new
            @rate.from_node = @user
            @rate.to_node = @truck
            @rate.general = 3
            @rate.save
            
            api_authorization_header @token.auth_token
        end
        
        after(:each) do
            @user.destroy
            @truck.destroy
        end
        
        context "when show user ratings" do
            before(:each) do
                get :show
            end
            
            it "returns the information about a reporter on a hash" do
                user_response = json_response
                #pp user_response
                expect(user_response[:ratings][0][:id]).to eq(@rate.id.to_s)
            end
            
            it { should respond_with 200 }
        end
        
        context "when show truck ratings" do
            before(:each) do
                get :show, { id: @truck.id }
            end
            
            it "returns the information about a reporter on a hash" do
                user_response = json_response
                expect(user_response[:ratings][0][:general]).to eq(@rate.general)
            end
            
            it { should respond_with 200 }
        end
        
    end
    
    describe "POST #create" do
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @truck = FactoryGirl.create :truck
            
            @token.create_rel("FOR", @user)
            
            api_authorization_header @token.auth_token
        end
        
        after(:each) do
            @user.destroy
            @truck.destroy
        end
        
        context "when is successfully created" do
            before(:each) do
                post :create, { id: @truck.id, comment: "comentário", general: 3 }
            end
            
            it { should respond_with 201 }
        end
        
        context "when is not successfully created" do
            before(:each) do
                post :create, { id: @truck.id, comment: "comentário" }
            end
            
            it { should respond_with 422 }
        end
    end
    
    describe "DELETE #destroy" do
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @truck = FactoryGirl.create :truck
            
            @rate = Rate.new
            @rate.from_node = @user
            @rate.to_node = @truck
            @rate.general = 3
            @rate.save
            
            @token.create_rel("FOR", @user)
            
            api_authorization_header @token.auth_token
        end
        
        after(:each) do
            @user.destroy
            @truck.destroy
        end
        
        context "when is succesfully destroyed" do
            before(:each) do
                delete :destroy, { id: @rate.id }
            end
            
            it { should respond_with 204 }
        end
        
    end

end
