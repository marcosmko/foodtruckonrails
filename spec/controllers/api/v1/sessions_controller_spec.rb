require 'spec_helper'
require 'pp'

describe Api::V1::SessionsController do
    
    describe "POST #create" do
        
        before(:each) do
            @user = FactoryGirl.create :user
            @truck = FactoryGirl.create :truck
            @user.create_rel("OWN", @truck)
        end
        
        after(:each) do
            @user.destroy
        end
        
        context "when the credentials are correct" do
            
            before(:each) do
                post :create, { login: @user.email, password: "12345678" }
            end
            
            it "returns the user record corresponding to the given credentials" do
                expect(json_response[:auth_token]).not_to be_empty
            end
            
            it "returns the (until now unique) truck id associated with user" do
                expect(json_response[:trucks].count).to be > 0
            end
            
            it { should respond_with 201 }
        end
        
        context "when the password is incorrect" do
            
            before(:each) do
                post :create, { login: @user.email, password: "invalidpassword" }
            end
            
            it { should respond_with 400 }
        end
        
        context "when the credentials are incorrect" do
            
            before(:each) do
                post :create, { login: "invalid@email.com", password: "invalidpassword" }
            end
            
            it { should respond_with 400 }
        end

    end
    
    describe "DELETE #destroy" do
        
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @token.create_rel("FOR", @user)
            api_authorization_header @token.auth_token
            delete :destroy
        end
        
        after(:each) do
            @user.destroy
        end
        
        it { should respond_with 204 }
        
    end

end