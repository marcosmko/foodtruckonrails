require 'spec_helper'
require 'pp'

describe Api::V1::UsersController do
    
#    describe "GET #show" do
#        before(:each) do
#            @user = FactoryGirl.create :user
#            @token = FactoryGirl.create :token
#            @token.create_rel("FOR", @user)
#            
#            api_authorization_header @token.auth_token
#            get :show, id: @user.email, format: :json
#        end
#        
#        after(:each) do
#            @user.destroy
#        end
#        
#        it "returns the information about a reporter on a hash" do
#            user_response = json_response
#            expect(user_response[:email]).to eql @user.email
#        end
#        
#        it { should respond_with 200 }
#    end

    describe "POST #create" do
        context "when is successfully created" do
            before(:each) do
                @user_attributes = FactoryGirl.attributes_for :user
                post :create, @user_attributes, format: :json
            end
            
            after(:each) do
                api_authorization_header json_response[:auth_token]
                delete :destroy
            end
            
            it "should have authentication code" do
                user_response = json_response
                expect(user_response[:auth_token]).not_to be_empty
            end
            
            it "should have trucks" do
                user_response = json_response
                expect(json_response[:trucks].count).to be > 0
            end
            
            it { should respond_with 201 }
        end
        
        context "when is not created" do
            before(:each) do
                @invalid_user_attributes = { password: "12345678", password_confirmation: "12345678" }
                post :create, { user: @invalid_user_attributes },
                format: :json
            end
            
            it { should respond_with 422 }
        end
    end

    describe "PUT/PATCH #update" do
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @token.create_rel("FOR", @user)
            
            api_authorization_header @token.auth_token
        end
        
        after(:each) do
            @user.destroy
        end
        
        context "when is successfully updated" do
            before(:each) do
                patch :update, { email: "newmail@example.com" }, format: :json
            end

            it { should respond_with 204 }
        end
        
        context "when is not created" do
            before(:each) do
                patch :update, { email: "bademail.com" }, format: :json
            end
            
            it "renders an errors json" do
                user_response = json_response
                expect(user_response).to have_key(:errors)
            end
            
            it "renders the json errors on whye the user could not be created" do
                user_response = json_response
                expect(user_response[:errors][:email]).to include "is invalid"
            end
            
            it { should respond_with 422 }
        end
    end
    
    describe "DELETE #destroy" do
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @token.create_rel("FOR", @user)
            
            api_authorization_header @token.auth_token
            delete :destroy, format: :json
        end
        
        it { should respond_with 204 }
        
    end

end