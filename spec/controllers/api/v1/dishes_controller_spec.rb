require 'rails_helper'
require 'pp'

RSpec.describe Api::V1::DishesController, type: :controller do
    
    describe "POST #create" do
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @truck = FactoryGirl.create :truck
            
            @token.create_rel("FOR", @user)
            @user.create_rel("OWN", @truck)
            
            api_authorization_header @token.auth_token
        end
        
        after(:each) do
            @user.destroy
        end
        
        context "when is successfully created" do
            before(:each) do
                @dish_attributes = FactoryGirl.attributes_for :dish
                post :create, {id: @truck.id, dish: @dish_attributes}, format: :json
            end
            
            it "renders an errors json" do
                user_response = json_response
                expect(user_response).to have_key(:id)
            end
            
            it { should respond_with 201 }
        end

        context "when truck not found" do
            before(:each) do
                @truck = FactoryGirl.create :truck
                @dish_attributes = FactoryGirl.attributes_for :dish
                post :create, {id: @truck.id, dish: @dish_attributes}, format: :json
            end
            
            after(:each) do
                @truck.destroy
            end
            
            it { should respond_with 403 }
        end
    end
    
    describe "PUT/PATCH #update" do
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @truck = FactoryGirl.create :truck
            @dish = FactoryGirl.create :dish
            
            @token.create_rel("FOR", @user)
            @user.create_rel("OWN", @truck)
            @truck.create_rel("HAS", @dish)
            
            api_authorization_header @token.auth_token
        end
        
        after(:each) do
            @user.destroy
        end
        
        context "when is successfully updated" do
            before(:each) do
                patch :update, { id: @dish.id, name: "NEW NAME DISH" }, format: :json
            end
            
            it { should respond_with 204 }
        end
        
        context "when is not created" do
            before(:each) do
                patch :update, { id: @dish.id, name: "" }, format: :json
            end
            
            it { should respond_with 422 }
        end
        
        context "when user does not have authorization" do
            before(:each) do
                @another_dish = FactoryGirl.create :dish
                patch :update, { id: @another_dish.id, name: "NEW NAME TRUCK" }, format: :json
            end
            
            after(:each) do
                @another_dish.destroy
            end
            
            it { should respond_with 403 }
        end
    end

    describe "DELETE #destroy" do
        before(:each) do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @truck = FactoryGirl.create :truck
            
            @token.create_rel("FOR", @user)
            @user.create_rel("OWN", @truck)
            
            api_authorization_header @token.auth_token
        end
        
        after(:each) do
            @user.destroy
        end
        
        context "when is succesfully destroyed" do
            before(:each) do
                @dish = FactoryGirl.create :dish
                @truck.create_rel("HAS", @dish)
                delete :destroy, { id: @dish.id }, format: :json
            end
            
            it { should respond_with 204 }
        end
        
        context "when user does not have authorization" do
            before(:each) do
                @dish = FactoryGirl.create :dish
                delete :destroy, { id: @dish.id }, format: :json
            end
            
            after(:each) do
                @dish.destroy
            end
            
            it { should respond_with 403 }
        end
        
    end

end
