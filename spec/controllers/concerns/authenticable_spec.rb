require 'spec_helper'
require 'pp'

class Authentication < ActionController::Base
    include Authenticable
end

describe Authenticable do
    let(:authentication) { Authentication.new }
    subject { authentication }
    
    describe "#current_user" do
        before do
            @user = FactoryGirl.create :user
            @token = FactoryGirl.create :token
            @token.create_rel("FOR", @user)
            request.headers["Authorization"] = @token.auth_token
            authentication.stub(:request).and_return(request)
        end
        
        after do
            @user.destroy
        end

        it "returns the user from the authorization header" do
            expect(authentication.current_token.user).to eql @user
        end
    end
    
    describe "#authenticate_with_token" do
        before do
            @user = FactoryGirl.create :user
            authentication.stub(:current_user).and_return(nil)
            response.stub(:response_code).and_return(401)
            response.stub(:body).and_return({"errors" => "Not authenticated"}.to_json)
            authentication.stub(:response).and_return(response)
        end
        
        after do
            @user.destroy
        end
        
        it "render a json error message" do
            expect(json_response[:errors]).to eql "Not authenticated"
        end
        
        it {  should respond_with 401 }
    end

end