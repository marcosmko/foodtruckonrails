require 'rails_helper'

RSpec.describe Schedule, type: :model do
    before {
        @schedule = FactoryGirl.build(:schedule)
    }
    
    subject { @schedule }
    
    it { should respond_to(:latitude) }
    it { should respond_to(:longitude) }
    it { should respond_to(:from) }
    it { should respond_to(:to) }
    it { should be_valid }
    
end
