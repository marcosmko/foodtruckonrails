require 'rails_helper'

RSpec.describe Truck, type: :model do
    before {
        @truck = FactoryGirl.build(:truck)
    }
    
    subject { @truck }
    
    it { should respond_to(:name) }
    it { should respond_to(:description) }
    it { should be_valid }
    it { should validate_presence_of(:name) }
    
end
