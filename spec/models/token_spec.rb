require 'rails_helper'

RSpec.describe Token, type: :model do
    before { @token = FactoryGirl.build(:token) }
    
    subject { @token }
    
    describe "#generate_authentication_token!" do
        it "generates a unique token" do
            Devise.stub(:friendly_token).and_return("auniquetoken123")
            @token.generate_authentication_token!
            expect(@token.auth_token).to eql "auniquetoken123"
        end
        
        it "generates another token when one already has been taken" do
            existing_token = FactoryGirl.create(:token, auth_token: "auniquetoken123")
            @token.generate_authentication_token!
            expect(@token.auth_token).not_to eql existing_token.auth_token
            existing_token.destroy
        end
    end
    
end
