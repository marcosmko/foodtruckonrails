require 'rails_helper'

RSpec.describe Dish, type: :model do
    before {
        @dish = FactoryGirl.build(:dish)
    }
    
    subject { @dish }
    
    it { should respond_to(:name) }
    it { should respond_to(:description) }
    it { should be_valid }
    it { should validate_presence_of(:name) }
    
end
