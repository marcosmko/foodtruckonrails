Truck.all.each do |truck|
    a = 0
    truck.ratings.each_rel { |r|
        a += r.general
    }
    truck.evaluators = truck.ratings.count
    truck.rate = a / Float(truck.evaluators)
    truck.save
end
