require File.expand_path('../boot', __FILE__)

require 'rails/all'
#require "action_controller/railtie"
#require "action_mailer/railtie"
#require "sprockets/railtie"
#require "rails/test_unit/railtie"

%w(
   neo4j
   action_controller
   action_mailer
   sprockets
   ).each do |framework|
    begin
        require "#{framework}/railtie"
        rescue LoadError
    end
   end

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Foodtruck
    class Application < Rails::Application
        # don't generate RSpec tests for views and helpers
        config.generators do |g|
            g.orm :neo4j
            g.test_framework :rspec, fixture: true
            g.fixture_replacement :factory_girl, dir: 'spec/factories'
            g.view_specs false
            g.helper_specs false
            g.stylesheets = false
            g.javascripts = false
            g.helper = false
        end
    
        config.autoload_paths += %W(\#{config.root}/lib)
        # Settings in config/environments/* take precedence over those specified here.
        # Application configuration should go into files in config/initializers
        # -- all .rb files in that directory are automatically loaded.
    
        # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
        # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
        # config.time_zone = 'Central Time (US & Canada)'
    
        # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
        # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
        # config.i18n.default_locale = :de
    
        # Do not swallow errors in after_commit/after_rollback callbacks.
        # config.active_record.raise_in_transactional_callbacks = true
        config.neo4j.session_type = :server_db
        config.neo4j.session_path = ENV['GRAPHENEDB_URL'] || 'http://neo4j:cajoluma@localhost:7474'
    end
end
