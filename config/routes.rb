require 'api_constraints'

Rails.application.routes.draw do
    root 'website/home#index'
    
    scope module: :website do
        post 'signup', to: 'home#create'
        
        get 'login', to: 'sessions#new'
        post 'login', to: 'sessions#create'
        
        get 'controlpanel', to: 'controlpanel#index'
        post 'controlpanel', to: 'controlpanel#update'
        
        post 'dishes', to: 'dishes#create'
        delete 'dishes', to: 'dishes#destroy'
        
        post 'uploader', to: 'uploader#create'
    end
    
    devise_for :users
    namespace :api, defaults: { format: :json }, constraints: { subdomain: 'api' }, path: '/'  do
        scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
            # We are going to list our resources here
            resource :users, :only => [:show, :create, :update, :destroy]
            resource :sessions, :only => [:create, :destroy]
            
            resources :categories, :only => [:show]
            resource :categories, :only => [:show]
            
            resources :trucks, :only => [:show]
            resource :trucks, :only => [:show, :update]
            
            resource :dishes, :only => [:create, :update, :destroy]
            resource :schedules, :only => [:show, :create, :update, :destroy]
            
            resource :wishlists, :only => [:show, :create, :destroy]
            resource :ratings, :only => [:show, :create, :destroy]
        end
    end
    
end
