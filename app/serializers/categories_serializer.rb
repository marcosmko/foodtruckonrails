class CategoriesSerializer < ActiveModel::Serializer
    attributes :id, :name, :image
end
