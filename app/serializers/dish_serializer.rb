class DishSerializer < ActiveModel::Serializer
    attributes :id, :name, :description, :price, :picture
    
    def picture
        return "trucks/#{object.truck.id}/dishes/#{object.picture.filename}"
    end
end
