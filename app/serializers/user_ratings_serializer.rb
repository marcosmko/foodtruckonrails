class UserRatingsSerializer < ActiveModel::Serializer
    attributes :id, :truck, :general, :comment
    
    def id
        return object.id.to_s
    end
    def truck
        dict = Hash.new
        dict[:id] = object.to_node.id
        dict[:name] = object.to_node.name
        dict[:avatar] = object.to_node.avatar.url
        dict[:cover] = object.to_node.cover.url
        return dict
    end
    
end
