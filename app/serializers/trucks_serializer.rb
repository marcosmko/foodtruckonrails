class TrucksSerializer < ActiveModel::Serializer
    attributes :id, :name, :avatar, :cover, :categories, :rate
    
    def attributes(*args)
        hash = super
        hash.each { |key, value|
            if value.nil?
                hash.delete(key)
            end
        }
        hash
    end
    
    def minEvaluators
        return 5
    end
    
    def categories
        if object.categories.present?
           return [object.categories.id]
        end
    end
    
    def avatar
        return object.avatar.url
    end
    
    def cover
        return object.cover.url
    end
    
    def rate
        if object.evaluators > minEvaluators
            return object.rate
        end
    end

end
