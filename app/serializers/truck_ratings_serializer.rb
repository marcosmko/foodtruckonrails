class TruckRatingsSerializer < ActiveModel::Serializer
    attributes :id, :general, :comment, :facebook
    
    def id
        return object.id.to_s
    end
    def facebook
        return object.from_node.facebook
    end
end
