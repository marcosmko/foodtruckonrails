class ScheduleSerializer < ActiveModel::Serializer
    attributes :latitude, :longitude, :from, :to
end
