class TruckSerializer < ActiveModel::Serializer
    attributes :description, :avatar, :cover, :name, :rate, :evaluators
    
    def attributes(*args)
        hash = super
        hash.each { |key, value|
            if value.nil?
                hash.delete(key)
            end
        }
        hash
    end
    
    def minEvaluators
        return 5
    end
    
    def avatar
        return object.avatar.url
    end
    def cover
        return object.cover.url
    end
    
    def rate
        if object.evaluators > minEvaluators
            return object.rate
        end
    end
    
    def evaluators
        if object.evaluators > minEvaluators
            return object.evaluators
        end
    end
    
end
