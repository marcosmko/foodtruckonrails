class Website::DishesController < WebsiteController
    
    def create
        
        begin
            truck = current_user.trucks.find(session[:truck_id])
            dish = Dish.new(dish_params)
            
            if params[:dish][:picture].present?
                dish.picture = params[:dish][:picture]
                dish.picture.truck_id = truck.id
            end
            
            if dish.save
                truck.create_rel("HAS", dish)
                redirect_to :back
            else
                head 422
            end
        rescue Neo4j::ActiveNode::Labels::RecordNotFound
            redirect_to '/login'
        end

    end
    
    def destroy

        begin
            truck = current_user.trucks.find(session[:truck_id])
            dish = truck.dishes.find(params[:id])
            
            dish.picture.truck_id = truck.id
            if dish.save && dish.destroy
                redirect_to :back
            else
                head 422
            end
        rescue Neo4j::ActiveNode::Labels::RecordNotFound
            redirect_to '/login'
        end

    end
    
    private
    def dish_params
        params[:dish].permit(:name, :description, :price)
    end
    
end
