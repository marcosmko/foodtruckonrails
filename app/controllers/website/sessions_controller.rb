class Website::SessionsController < WebsiteController
    
    def new
        if logged_in?
            redirect_to '/controlpanel'
        else
            render "login.html.erb"
        end
    end
    
    def create
        user_login = params[:session][:login]
        user_password = params[:session][:password]

        user = user_login.present? && User.find_by(username: user_login)
        if user.nil?
            user = user_login.present? && User.find_by(email: user_login)
        end

        if user.nil?
            new
        elsif user.valid_password? user_password
            token = Token.new
            token.save
            
            token.create_rel("FOR", user)
            
            log_in token
            redirect_to controlpanel_url
        else
            new
        end
    end
    
end
