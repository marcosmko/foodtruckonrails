class Website::ControlpanelController < WebsiteController
    
    def index
        if logged_in?
            current_truck
            render 'controlpanel.html.erb'
        else
            redirect_to '/login'
        end
    end
    
    def update
        
        begin
            truck = current_user.trucks.find(session[:truck_id])
            
            if params[:truck][:category].present? &&
                (truck.categories.nil? || truck.categories.id != params[:truck][:category])
                
                truck.categories = Category.find(params[:truck][:category])
                truck.save
            end
            
            if truck.update(params[:truck])
                index
            else
                redirect_to '/login'
            end
        rescue Neo4j::ActiveNode::Labels::RecordNotFound
            redirect_to '/login'
        end

    end
    
    def current_truck
        @current_truck ||=  current_user.trucks.first
        session[:truck_id] ||= @current_truck.id
    end
    
end
