class Website::HomeController < WebsiteController
    
    def index
        render "home.html.erb"
    end
    
    def create
        if params[:consumer].present?
            
            register = Register.new(params[:consumer])
            if register.save
                respond_to do |format|
                    format.html  { redirect_to :back, notice: 'Cadastrado com sucesso! Obrigado!' }
                end
                UserMailer.welcome_email_user(register.as_json).deliver_later
            else
                respond_to do |format|
                    format.html  { redirect_to :back, notice: 'Erro. Tente novamente!' }
                end
            end
            
        elsif params[:truck].present?
            
            register = Register.new(params[:truck])
            if register.save
                respond_to do |format|
                    format.html  { redirect_to :back, notice: 'Cadastrado com sucesso! Entraremos em contato!' }
                end
                UserMailer.welcome_email_truck(register.as_json).deliver_later
            else
                respond_to do |format|
                    format.html  { redirect_to :back, notice: 'Erro. Tente novamente!' }
                end
            end
            
        else
            redirect_to :back
        end
    end
    
end
