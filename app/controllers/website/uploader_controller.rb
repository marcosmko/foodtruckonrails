class Website::UploaderController < WebsiteController
    
    def create
        if params[:truck][:avatar].present?
            current_truck.avatar = params[:truck][:avatar]
            current_truck.avatar.rename = "avatar"
            
            current_truck.save!
            render json: { truck: current_truck.avatar.url }
        elsif params[:truck][:cover].present?
            current_truck.cover = params[:truck][:cover]
            current_truck.cover.rename = "cover"
            current_truck.save!
            
            render json: { truck: current_truck.cover.url }
        else
            head 400
        end
    end
    
    def current_truck
        @current_truck ||=  current_user.trucks.find(session[:truck_id])
    end
    
end
