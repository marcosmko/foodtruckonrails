module Authenticable
    
    # Devise methods overwrites
    
    def current_token
        @current_token = Token.find_by(auth_token: request.headers['Authorization'])
    end
    
    def current_user
        if current_token.present?
            @current_user = current_token.user
        else
            @current_user = nil
        end
    end
    
    def authenticate_with_token!
        head 401 unless user_signed_in?
    end
    
    def user_signed_in?
        current_user.present?
    end
    
end