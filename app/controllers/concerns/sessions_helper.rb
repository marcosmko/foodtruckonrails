module SessionsHelper
    
    # Logs in the given user.
    def log_in(token)
        session[:auth_token] = token.auth_token
    end
    
    def current_token
        @current_token ||= Token.find_by(auth_token: session[:auth_token])
    end
    
    # Returns the current logged-in user (if any).
    def current_user
        if current_token.present?
            @current_user ||= current_token.user
        end
    end
    
    # Returns true if the user is logged in, false otherwise.
    def logged_in?
        !current_user.nil?
    end
    
end
