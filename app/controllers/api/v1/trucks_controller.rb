class Api::V1::TrucksController < ApplicationController
    before_action :authenticate_with_token!, only: [:update]
    respond_to :json
    
    def show
        if params[:id].present?
            begin
                truck = Truck.find(params[:id])
                
                truck_json = TruckSerializer.new(truck).as_json
                if truck.dishes.count > 0
                    truck_json[:dishes] = ActiveModel::Serializer::CollectionSerializer.new(truck.dishes, serializer: DishSerializer)
                end

                schedules = truck.schedules(:s).where("{time} <= s.to").params(time: Time.new.to_i)
                if schedules.count > 0
                    truck_json[:schedules] = ActiveModel::Serializer::CollectionSerializer.new(schedules, serializer: ScheduleSerializer)
                end
                
                render json: truck_json
            rescue Neo4j::ActiveNode::Labels::RecordNotFound
                head 404
            end
        elsif params[:name].present?
            render json: { trucks: ActiveModel::Serializer::CollectionSerializer.new(Truck.where(name: /(?i)#{params[:name]}.*/).limit(5), serializer: TrucksSerializer) }
        else
            render json: { trucks: ActiveModel::Serializer::CollectionSerializer.new(Truck.all, serializer: TrucksSerializer) }
        end
    end
    
    def update
        begin
            truck = current_user.trucks.find(params[:id])
            if truck.update(params[:truck])
                head 204
            else
                head 422
            end
        rescue Neo4j::ActiveNode::Labels::RecordNotFound
            head 403
        end
    end
    
end
