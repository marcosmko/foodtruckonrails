class Api::V1::SessionsController < ApplicationController
    
    def create
        
        facebook = params[:facebook]
        if facebook.present?
            
            user = User.find_by(facebook: facebook)
            if user.nil?
                user = User.new(facebook_params)
                user.password = SecureRandom.urlsafe_base64(nil, false)
                
                if user.save
                    token = Token.new
                    token.save
                    token.create_rel("FOR", user)
                    
                    render json: { auth_token: token.auth_token }
                else
                    head 423
                end
                
            else
            
                token = Token.new
                token.save
                token.create_rel("FOR", user)
                
                render json: { auth_token: token.auth_token }
                
            end
            
        else
            head 423
        end
#        render json: params
#        user_login = params[:login]
#        user_password = params[:password]
#        
#        user = user_login.present? && User.find_by(username: user_login)
#        if user.nil?
#            user = user_login.present? && User.find_by(email: user_login)
#        end
#        
#        if user.nil?
#            head 400
#        elsif user.valid_password? user_password
#            #sign_in user, store: false #what is it?
#            
#            token = Token.new
#            token.save
#            
#            token.create_rel("FOR", user)
#            
#            props = UserSerializer.new(user).as_json
#            props[:auth_token] = token.auth_token
#            
#            array = []
#            user.trucks.each do |dict|
#                array << TrucksSerializer.new(dict).as_json
#            end
#            props[:trucks] = array
#
#            render json: props, status: 201
#        else
#            head 400
#        end
    end
    
    def destroy
        current_token.destroy
        head 204
    end
    
    private
    def facebook_params
        params[:session].permit(:facebook, :email, :username, :password)
    end
    
end
