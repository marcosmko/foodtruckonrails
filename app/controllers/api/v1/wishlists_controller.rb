class Api::V1::WishlistsController < ApplicationController
    before_action :authenticate_with_token!, only: [:show, :create, :destroy]
    
    def show
        render json: { trucks: ActiveModel::Serializer::CollectionSerializer.new(current_user.wishlist, serializer: TrucksSerializer) }
    end
    
    def create
        truck = Truck.find(params[:id])
        current_user.wishlist << truck
        head 201
    end
    
    def destroy
        truck = Truck.find(params[:id])
        current_user.wishlist.delete(truck)
        head 204
    end
    
end
