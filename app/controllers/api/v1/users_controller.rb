class Api::V1::UsersController < ApplicationController
    before_action :authenticate_with_token!, only: [:show, :update, :destroy]
    respond_to :json
    
    #should stay deactivated, or only visible for admins
    #def show
    #    respond_with User.find_by(email: params[:id])
    #end

    def create
        user = User.new(user_params)
        if user.save
            token = Token.new
            truck = Truck.new(name:"Sem Nome")
            if token.save && truck.save
                token.create_rel("FOR", user)
                user.create_rel("OWN", truck)
                
                props = token.props
                
                array = [TrucksSerializer.new(truck).as_json]
                props[:trucks] = array

                render json: props, status: 201
            else
                user.destroy
                head 500
            end
        else
            head 422
        end
    end
    
    #return that request was succeeded
    def update
        user = current_user
        if user.update(user_params)
            head 204
        else
            render json: { errors: user.errors }, status: 422
        end
    end
    
    #should be deactivated!!
    def destroy
        current_user.trucks.each do |truck|
            truck.destroy
        end
        current_user.destroy
        head 204
    end
    
    private
    def user_params
        params.permit(:email, :password, :password_confirmation, :username)
    end
end