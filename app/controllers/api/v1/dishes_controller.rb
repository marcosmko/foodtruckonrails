class Api::V1::DishesController < ApplicationController
    before_action :authenticate_with_token!, only: [:create, :update, :destroy]
    
    def create
        begin
            truck = current_user.trucks.find(params[:id])
            dish = Dish.new(dish_params)
            
            if dish.save
                truck.create_rel("HAS", dish)
                render json: { id: dish.id }, status: 201
            else
                head 422
            end
        rescue Neo4j::ActiveNode::Labels::RecordNotFound
            head 403
        end
    end
    
    def update
        begin
            dish = current_user.trucks.dishes.find(params[:id])
            if dish.update(dish_params)
                head 204
            else
                head 422
            end
        rescue Neo4j::ActiveNode::Labels::RecordNotFound
            head 403
        end
    end
    
    def destroy
        begin
            dish = current_user.trucks.dishes.find(params[:id])
            dish.destroy
            head 204
        rescue Neo4j::ActiveNode::Labels::RecordNotFound
            head 403
        end
    end
    
    private
    def dish_params
        params[:dish].permit(:name, :description, :price)
    end
    
end
