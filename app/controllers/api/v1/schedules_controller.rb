class Api::V1::SchedulesController < ApplicationController
    before_action :authenticate_with_token!, only: [:create, :update, :destroy]
    
    def show
        begin
            if params[:from].present? && params[:to].present?
                from = Time.parse(params[:from])
                to = Time.parse(params[:to])
                
                schedules = Schedule.all.where("n.to > {from} AND n.from < {to}").params(from: from.to_i, to: to.to_i)
            elsif params[:time].present?
                time = Time.parse(params[:time])
                schedules = Schedule.all.where("n.from <= {time} AND {time} <= n.to").params(time: time.to_i)
            else
                schedules = Schedule.all.where("n.from <= {time} AND {time} <= n.to").params(time: Time.new.to_i)
            end
            
            dict = Hash.new
            schedules.each do |schedule|
                id = schedule.truck.id
                schedule = ScheduleSerializer.new(schedule).as_json
                
                if dict[id].nil?
                    dict[id] = []
                end
                
                dict[id] << schedule
            end
            
            array = []
            schedules.truck.each do |bla|
                truck = TrucksSerializer.new(bla).as_json
                truck[:schedules] = dict[bla.id]
                array << truck
            end
            
            render json: { trucks: array }
        rescue
            head 422
        end
    end
    
    def create
        begin
            truck = current_user.trucks.find(params[:id])
            
            params[:schedule][:from] = Time.parse(params[:schedule][:from])
            params[:schedule][:to] = Time.parse(params[:schedule][:to])
            
            schedule = Schedule.new(schedule_params)
            if schedule.save
                truck.create_rel("WITH", schedule)
                render json: { id: schedule.id }, status: 201
            else
                head 422
            end
        rescue Neo4j::ActiveNode::Labels::RecordNotFound
            head 403
        rescue
            head 422
        end
    end
    
    def update
        begin
            schedule = current_user.trucks.schedules.find(params[:id])
            
            from = params[:schedule][:from]
            to = params[:schedule][:to]
            
            if from.present?
                params[:schedule][:from] = Time.parse(from)
            end
            if to.present?
                params[:schedule][:to] = Time.parse(to)
            end
            
            if schedule.update(schedule_params)
                head 204
            else
                head 422
            end
        rescue Neo4j::ActiveNode::Labels::RecordNotFound
            head 403
        rescue
            head 422
        end
    end
    
    def destroy
        begin
            schedule = current_user.trucks.schedules.find(params[:id])
            schedule.destroy
            head 204
        rescue Neo4j::ActiveNode::Labels::RecordNotFound
            head 403
        end
    end
    
    private
    def schedule_params
        params[:schedule].permit(:latitude, :longitude, :from, :to)
    end

end
