class Api::V1::CategoriesController < ApplicationController
    respond_to :json
    
    def show
        if params[:id].present?
            render json: { trucks: ActiveModel::Serializer::CollectionSerializer.new(Category.find(params[:id]).trucks, serializer: TrucksSerializer) }
        else
            render json: { categories: ActiveModel::Serializer::CollectionSerializer.new(Category.all, serializer: CategoriesSerializer) }
        end
    end
    
end
