class Api::V1::RatingsController < ApplicationController
    before_action :authenticate_with_token!, only: [:create, :destroy]

    def show
        if params[:id].present?
            truck = Truck.find(params[:id])
            render json: { ratings: ActiveModel::Serializer::CollectionSerializer.new(truck.ratings.each_rel, serializer: TruckRatingsSerializer) }
        else
            authenticate_with_token!
            render json: { ratings: ActiveModel::Serializer::CollectionSerializer.new(current_user.ratings.each_rel, serializer: UserRatingsSerializer) }
        end
    end
    
    def create
        truck = Truck.find(params[:id])
        
        rate = Rate.new(rating_params)
        rate.to_node = truck
        rate.from_node = current_user
        
        if rate.save
            truck.rate = (truck.rate * truck.evaluators) / (truck.evaluators + 1)
            truck.evaluators += 1
            truck.save
            
            render json: { id: rate.id.to_s }, status: 201
        else
            head 422
        end
    end
    
    def destroy
        rate = Rate.find(params[:id])
        if rate.from_node == current_user
            rate.destroy
            head 204
        else
            head 403
        end
    end
    
    private
    def rating_params
        params[:rating].permit(:general, :comment)
    end
    
end
