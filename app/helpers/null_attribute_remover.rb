module NullAttributesRemover
    def attributes(*args)
        hash = super
        hash.each { |key, value|
            if value.nil?
                hash.delete(key)
            end
        }
        hash
    end
end