class Rate include Neo4j::ActiveRel
    
    from_class :User
    to_class   :Truck
    
    type 'RATE'
    
    property :general, type: Integer, null: false
    property :comment, type: String, null: false, default: ""

    validates_presence_of :general
    
end
