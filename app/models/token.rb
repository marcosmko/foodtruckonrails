class Token include Neo4j::ActiveNode
    
    property :auth_token, type: String, null: false, default: ""
    index :auth_token
    
    has_one :out, :user, type: 'FOR', model_class: 'User'
    
    before_create :generate_authentication_token!
    def generate_authentication_token!
        # begin
        self.auth_token = Devise.friendly_token
        # end while self.class.exists?(auth_token: auth_token)
    end
    
end
