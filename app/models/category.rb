class Category include Neo4j::ActiveNode
    
    property :name, type: String, null: false
    property :image, type: String, null: false
    
    has_many :out, :trucks, type: 'CONTAIN', model_class: 'Truck'

end
