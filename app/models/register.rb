class Register include Neo4j::ActiveNode

    #properties
    property :name, type: String
    property :email, type: String, null: false
    property :truck, type: String
    property :phonenumber, type: String
    property :os, type: Integer
    
    #validation
    validates_presence_of :email

end
