class User include Neo4j::ActiveNode
    #
    # Neo4j.rb needs to have property definitions before any validations. So, the property block needs to come before
    # loading your devise modules.
    #
    # If you add another devise module (such as :lockable, :confirmable, or :token_authenticatable), be sure to
    # uncomment the property definitions for those modules. Otherwise, the unused property definitions can be deleted.
    #
    
    # validates :auth_token, uniqueness: true

    #properties
    property :facebook, type: String
    property :email, type: String
    property :username, type: String
    
    property :encrypted_password
    
    property :created_at, type: DateTime
    property :updated_at, type: DateTime
    
    #indexes
    index :email
    index :facebook
    index :username
    
    #outlets
    has_many :in, :auth_tokens, type: 'FOR', model_class: 'Token', dependent: :delete
    has_many :out, :trucks, type: 'OWN', model_class: 'Truck', dependent: :delete
    
    has_many :out, :wishlist, type: 'WISH', model_class: 'Truck', unique: true
    has_many :out, :ratings, rel_class: :Rate
    
    #validation
#    validates_presence_of :username
#    validates_uniqueness_of :username

    #redefinition, destroy all trucks and respective dishes
    def destroy
        trucks.destroy_all
        super
    end
    
    
    
    
    ## If you include devise modules, uncomment the properties below.
    ## Rememberable
    property :remember_created_at, type: DateTime
    property :remember_token
    index :remember_token
    
    
    ## Recoverable
    property :reset_password_token
    index :reset_password_token
    property :reset_password_sent_at, type: DateTime
    
    ## Trackable
    property :sign_in_count, type: Integer, default: 0
    property :current_sign_in_at, type: DateTime
    property :last_sign_in_at, type: DateTime
    property :current_sign_in_ip, type:  String
    property :last_sign_in_ip, type: String
    
    ## Confirmable
    # property :confirmation_token
    # index :confirmation_token
    # property :confirmed_at, type: DateTime
    # property :confirmation_sent_at, type: DateTime
    
    ## Lockable
    #  property :failed_attempts, type: Integer, default: 0
    # property :locked_at, type: DateTime
    #  property :unlock_token, type: String,
    # index :unlock_token
    
    ## Token authenticatable
    # property :authentication_token, type: String, null: true, index: :exact
    
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable,
    :recoverable, :rememberable, :trackable, :validatable
    
    # validates_uniqueness_of :email, case_insensitive: true
    
#    before_create :generate_authentication_token!
#    def generate_authentication_token!
#        # begin
#        self.auth_token = Devise.friendly_token
#        # end while self.class.exists?(auth_token: auth_token)
#    end

    def email_required?
        true unless facebook.present?
    end

end
