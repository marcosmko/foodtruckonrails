class Schedule include Neo4j::ActiveNode
    
    #properties
    property :latitude, type: Float, null: false
    property :longitude, type: Float, null: false
    property :from, type: Time, null: false
    property :to, type: Time, null: false
    
    #indexes
    
    #outlets
    has_one :in, :truck, type: 'WITH', model_class: 'Truck'
    
end
