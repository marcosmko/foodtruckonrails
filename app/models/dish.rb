class Dish include Neo4j::ActiveNode
    
    #properties
    property :name, type: String, null: false, default: ""
    property :description, type: String, null: false, default: ""
    property :price, type: Float
    property :picture, type: String
    
    #outlets
    has_one :in, :truck, type: 'HAS', model_class: 'Truck'
    
    #validation
    validates_presence_of :name
    
    #uploaders
    mount_uploader :picture, DishUploader
    
end
