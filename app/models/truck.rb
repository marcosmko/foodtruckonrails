class Truck include Neo4j::ActiveNode
    
    #properties
    property :name, type: String, null: false, default: ""
    property :description, type: String, null: false, default: ""
    property :avatar, type: String
    property :cover, type: String
    
    property :rate, type: Float, null:false, default: 0
    property :evaluators, type: Integer, null:false, default: 0
    
    #indexes
    index :name
    
    #outlets
    has_one :in, :user, type: 'OWN', model_class: 'User'
    has_one :in, :categories, type: 'CONTAIN', model_class: 'Category'
    has_many :out, :dishes, type: 'HAS', model_class: 'Dish', dependent: :delete
    has_many :out, :schedules, type: 'WITH', model_class: 'Schedule', dependent: :delete
    
    has_many :in, :ratings, rel_class: :Rate
    
    #validation
    validates_presence_of :name
    
    #uploaders
    mount_uploader :avatar, TruckUploader
    mount_uploader :cover, TruckUploader
    
end
