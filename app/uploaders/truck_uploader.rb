class TruckUploader < CarrierWave::Uploader::Base
    storage :file
    
    attr_accessor :rename, :store
    def filename
        if rename.present?
            @name ||= "#{rename}.#{file.extension}"
        else
            @name ||= original_filename
        end
    end
    
    def store_dir
        "trucks/#{model.id}"
    end
end
