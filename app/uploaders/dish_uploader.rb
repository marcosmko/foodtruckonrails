# encoding: utf-8

class DishUploader < CarrierWave::Uploader::Base
    storage :file
    attr_accessor :truck_id
    
    def filename
        model.id ? "#{model.id}.#{file.extension}" : "#temp.#{file.extension}"
    end
    
    def store_dir
        "trucks/#{truck_id}/dishes"
    end
end
