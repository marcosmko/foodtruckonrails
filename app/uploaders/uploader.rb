class Uploader < CarrierWave::Uploader::Base
    storage :file
    
    attr_accessor :rename, :store
    def filename
        if rename.present?
            @name ||= "#{rename}.#{file.extension}"
        else
            @name ||= original_filename
        end
    end
    
    
    def store_dir
        if store.present?
            store
        else
            "content/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
        end
    end
end
