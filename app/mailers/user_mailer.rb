require 'pp'
class UserMailer < ApplicationMailer
    default from: 'marcosmko@gmail.com'
    
    def welcome_email_user(register)
        @register = register
        mail(to: @register["register"]["email"], subject: 'Cadastro realizado com sucesso!')
    end
    
    def welcome_email_truck(register)
        @register = register
        mail(to: @register["register"]["email"], subject: 'Cadastro realizado com sucesso!')
    end
    
end
